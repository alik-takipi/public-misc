package alik.war.resources;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import alik.agent.AlikApp246;
import alik.agent.AlikApp247;
import alik.agent.AlikAppNew246;
import alik.agent.AlikAppNew247;
import alik.war.greeting.Greeting;

@Path("/hello")
public class HelloResource
{
	@GET
	@Path("/11/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Greeting hello11(@PathParam("param") String name) throws Exception
	{
		// Comment to check if source was found
		new AlikApp246().getGreeting();
		return new Greeting(name);
	}
	
	@GET
	@Path("/12/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Greeting hello12(@PathParam("param") String name) throws Exception
	{
		// Comment to check if source was found
		new AlikAppNew246().getGreeting();
		return new Greeting(name);
	}
	
	@GET
	@Path("/21/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Greeting hello21(@PathParam("param") String name) throws Exception
	{
		// Comment to check if source was found
		new AlikApp247().getGreeting();
		return new Greeting(name);
	}
	
	@GET
	@Path("/22/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Greeting hello22(@PathParam("param") String name) throws Exception
	{
		// Comment to check if source was found
		new AlikAppNew247().getGreeting();
		return new Greeting(name);
	}
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public String helloUsingJson(Greeting greeting) {
		return greeting.getMessage() + "\n";
	}
}
